<?php namespace Keios\StudioManager\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateAlbumsTable
 * @package Keios\Studiomanager\Updates
 */
class CreateAlbumsTable extends Migration
{

    public function up()
    {
        Schema::create('keios_studiomanager_albums', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('artist_id');
            $table->string('slug');
            $table->string('title')->index();
            $table->longText('description');
            $table->longText('tracklist');
            $table->date('release_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_studiomanager_albums');
    }

}
