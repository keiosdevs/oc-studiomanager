<?php namespace Keios\StudioManager\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateArtistsTable
 * @package Keios\Studiomanager\Updates
 */
class CreateArtistsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'keios_studiomanager_artists',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('slug');
                $table->string('name')->index();
                $table->integer('country_id')->unsigned();
                $table->string('city');
                $table->longText('bio');
                $table->string('facebook')->nullable();
                $table->string('instagram')->nullable();
                $table->string('twitter')->nullable();
                $table->string('website')->nullable();
                $table->string('youtube')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_studiomanager_artists');
    }

}
