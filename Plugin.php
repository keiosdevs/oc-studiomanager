<?php namespace Keios\StudioManager;

use Backend\Facades\Backend;
use System\Classes\PluginBase;

/**
 * StudioManager Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.plugin.name',
            'description' => 'keios.studiomanager::lang.plugin.description',
            'author'      => 'keios',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Keios\StudioManager\Components\ArtistsComponent' => 'artists_component',
            'Keios\StudioManager\Components\AlbumsComponent'  => 'albums_component',
            'Keios\StudioManager\Components\ArtistComponent'  => 'artist_component',
            'Keios\StudioManager\Components\AlbumComponent'   => 'album_component',
        ];
    }

    /**
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'studiomanager' => [
                'label'    => 'keios.studiomanager::lang.messages.studiomanager',
                'url'      => Backend::url('keios/studiomanager/albums'),
                'icon'     => 'icon-music',
                'order'    => 500,
                'sideMenu' => [
                    'albums'  => [
                        'label' => 'keios.studiomanager::lang.messages.albums',
                        'url'   => Backend::url('keios/studiomanager/albums'),
                        'icon'  => 'icon-music',
                    ],
                    'artists' => [
                        'label' => 'keios.studiomanager::lang.messages.artists',
                        'url'   => Backend::url('keios/studiomanager/artists'),
                        'icon'  => 'icon-users',
                    ],
                ],
            ],
        ];
    }
}
