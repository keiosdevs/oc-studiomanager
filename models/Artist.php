<?php namespace Keios\StudioManager\Models;

use Model;

/**
 * artist Model
 */
class Artist extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_studiomanager_artists';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'albums' => 'Keios\StudioManager\Models\Album'
    ];
    public $belongsTo = [
        'country' => 'Keios\ProUser\Models\Country'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'photos' => ['System\Models\File'],
    ];

}