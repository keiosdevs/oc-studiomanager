<?php namespace Keios\StudioManager\Models;

use Model;

/**
 * album Model
 */
class Album extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_studiomanager_albums';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'artist' => 'Keios\StudioManager\Models\Artist',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'cover_images' => ['System\Models\File'],
    ];



}