<?php namespace Keios\StudioManager\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Database\Eloquent\Collection;
use Keios\StudioManager\Models\Album;
use Keios\StudioManager\Models\Artist;

/**
 * Class AlbumsComponent
 * @package Keios\Studiomanager\Components
 */
class AlbumsComponent extends ComponentBase
{

    /**
     * @var int
     */
    protected $pageParam;

    /**
     * @var string
     */
    protected $noAlbumMessage;

    /**
     * @var string
     */
    protected $albumPage;

    /**
     * @var string
     */
    protected $artistPage;

    /**
     * @var Artist
     */
    protected $artist;

    /**
     * @var Collection
     */
    protected $albums;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.components.albumscomponent.name',
            'description' => 'keios.studiomanager::lang.components.albumscomponent.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'pageNumber'      => [
                'title'       => 'keios.studiomanager::lang.settings.albums_pagination',
                'description' => 'keios.studiomanager::lang.settings.albums_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'artistFilter'    => [
                'title'       => 'keios.studiomanager::lang.settings.artists_filter',
                'description' => 'keios.studiomanager::lang.settings.artists_filter_description',
                'type'        => 'string',
                'default'     => '',
            ],
            'albumsPerPage'   => [
                'title'             => 'keios.studiomanager::lang.settings.albums_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'keios.studiomanager::lang.settings.albums_per_page_validation',
                'default'           => '10',
            ],
            'noAlbumsMessage' => [
                'title'             => 'keios.studiomanager::lang.settings.no_albums',
                'description'       => 'keios.studiomanager::lang.settings.no_albums_description',
                'type'              => 'string',
                'default'           => 'No albums found',
                'showExternalParam' => false,
            ],
            'artistPage'      => [
                'title'       => 'keios.studiomanager::lang.settings.category_page',
                'description' => 'keios.studiomanager::lang.settings.category_page_desc',
                'type'        => 'dropdown',
                'default'     => 'studiomanager/artist',
                'group'       => 'Links',
            ],
            'albumPage'       => [
                'title'       => 'keios.studiomanager::lang.settings.album_page',
                'description' => 'keios.studiomanager::lang.settings.album_page_desc',
                'type'        => 'dropdown',
                'default'     => 'studiomanager/album',
                'group'       => 'Links',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getArtistPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('url', 'url');
    }

    /**
     * @return mixed
     */
    public function getAlbumPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('url', 'url');
    }

    /**
     *
     */
    public function onRun()
    {
        $this->prepareVars();
        $this->page['albumPage'] = $this->parseUrl($this->albumPage);
        $this->page['artistPage'] = $this->parseUrl($this->artistPage);
    }

    /**
     *
     */
    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->noAlbumMessage = $this->page['noAlbumMessage'] = $this->property('noAlbumMessage');
        $this->albumPage = $this->property('albumPage');
        $this->artistPage = $this->property('artistPage');

        $this->albums = $this->page['albums'] = $this->listAlbums();
    }

    /**
     * @param $page
     *
     * @return mixed
     */
    protected function parseUrl($page)
    {
        return str_replace('/:slug', '', $page);
    }

    /**
     * @return mixed
     */
    protected function listAlbums()
    {
        return Album::with('artist')->get();
    }

}