<?php namespace Keios\Studiomanager\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Keios\StudioManager\Models\Artist;

/**
 * Class ArtistComponent
 * @package Keios\Studiomanager\Components
 */
class ArtistComponent extends ComponentBase
{

    /**
     * @var Artist
     */
    protected $artist;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $artistPage;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.components.artistcomponent.name',
            'description' => 'keios.studiomanager::lang.components.artistcomponent.description'
        ];
    }


    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'slug'       => [
                'title'       => 'keios.studiomanager::lang.settings.album_slug',
                'description' => 'keios.studiomanager::lang.settings.album_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'albumPage' => [
                'title'       => 'keios.studiomanager::lang.settings.album_page',
                'description' => 'keios.studiomanager::lang.settings.album_page_desc',
                'type'        => 'dropdown',
                'default'     => 'studiomanager/album',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getAlbumPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('url', 'url');
    }


    /**
     *
     */
    public function onRun()
    {
        $this->prepareVars();
        $this->getArtist($this->slug);

        $this->page['artist'] = $this->artist;
    }


    /**
     *
     */
    protected function prepareVars()
    {
        $this->slug = $this->page['pageParam'] = $this->property('slug');
        $this->artistPage = $this->page['categoryPage'] = $this->property('artistPage');
    }

    /**
     * @param $slug
     */
    protected function getArtist($slug)
    {
        $this->artist = Artist::where('slug', $slug)->first();
    }

}