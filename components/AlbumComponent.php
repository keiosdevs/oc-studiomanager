<?php namespace Keios\Studiomanager\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Keios\StudioManager\Models\Album;

/**
 * Class AlbumComponent
 * @package Keios\Studiomanager\Components
 */
class AlbumComponent extends ComponentBase
{
    /**
     * @var Album
     */
    protected $album;

    /**
     * @var string
     */
    protected $artistPage;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.components.albumcomponent.name',
            'description' => 'keios.studiomanager::lang.components.albumcomponent.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'slug'       => [
                'title'       => 'keios.studiomanager::lang.settings.album_slug',
                'description' => 'keios.studiomanager::lang.settings.album_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ],
            'artistPage' => [
                'title'       => 'keios.studiomanager::lang.settings.artist_page',
                'description' => 'keios.studiomanager::lang.settings.artist_page_desc',
                'type'        => 'dropdown',
                'default'     => 'studiomanager/category',
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getArtistPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('url', 'url');
    }


    /**
     *
     */
    public function onRun()
    {
        $this->prepareVars();
        $this->getAlbum($this->slug);

        $this->page['album'] = $this->album;
    }


    /**
     *
     */
    protected function prepareVars()
    {
        $this->slug = $this->page['pageParam'] = $this->property('slug');
        $this->artistPage = $this->page['categoryPage'] = $this->property('artistPage');
    }

    protected function getAlbum($slug)
    {
        $this->album = Album::where('slug', $slug)->first();
    }
}