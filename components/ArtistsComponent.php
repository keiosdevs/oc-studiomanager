<?php namespace Keios\StudioManager\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Database\Eloquent\Collection;
use Keios\StudioManager\Models\Artist;

/**
 * Class ArtistsComponent
 * @package Keios\Studiomanager\Components
 */
class ArtistsComponent extends ComponentBase
{

    /**
     * @var int
     */
    protected $pageParam;

    /**
     * @var string
     */
    protected $noArtistMessage;

    /**
     * @var string
     */
    protected $albumPage;

    /**
     * @var string
     */
    protected $artistPage;

    /**
     * @var Collection
     */
    protected $artists;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.studiomanager::lang.components.artistscomponent.name',
            'description' => 'keios.studiomanager::lang.components.artistscomponent.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'pageNumber'       => [
                'title'       => 'keios.studiomanager::lang.settings.albums_pagination',
                'description' => 'keios.studiomanager::lang.settings.albums_pagination_description',
                'type'        => 'string',
                'default'     => '{{ :page }}',
            ],
            'artistsPerPage'   => [
                'title'             => 'keios.studiomanager::lang.settings.artists_per_page',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'keios.studiomanager::lang.settings.artists_per_page_validation',
                'default'           => '10',
            ],
            'noArtistsMessage' => [
                'title'             => 'keios.studiomanager::lang.settings.no_artists',
                'description'       => 'keios.studiomanager::lang.settings.no_artists_description',
                'type'              => 'string',
                'default'           => 'No albums found',
                'showExternalParam' => false,
            ],
            'artistPage'       => [
                'title'       => 'keios.studiomanager::lang.settings.category_page',
                'description' => 'keios.studiomanager::lang.settings.category_page_desc',
                'type'        => 'dropdown',
                'default'     => 'artist',
                'group'       => 'Links',
            ],
            'albumPage'        => [
                'title'       => 'keios.studiomanager::lang.settings.album_page',
                'description' => 'keios.studiomanager::lang.settings.album_page_desc',
                'type'        => 'dropdown',
                'default'     => 'album',
                'group'       => 'Links',
            ],
        ];
    }


    /**
     * @return mixed
     */
    public function getArtistPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('url', 'url');
    }

    /**
     * @return mixed
     */
    public function getAlbumPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('url', 'url');
    }

    /**
     *
     */
    public function onRun()
    {
        $this->prepareVars();
        $this->page['albumPage'] = $this->parseUrl($this->albumPage);
        $this->page['artistPage'] = $this->parseUrl($this->artistPage);
    }

    /**
     *
     */
    protected function prepareVars()
    {
        $this->pageParam = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->noArtistMessage = $this->page['noArtistMessage'] = $this->property('noArtistMessage');
        $this->albumPage = $this->property('albumPage');
        $this->artistPage = $this->property('artistPage');

        $this->artists = $this->page['artists'] = $this->listArtists();
    }

    /**
     * @param $page
     *
     * @return mixed
     */
    protected function parseUrl($page)
    {
        return str_replace('/:slug', '', $page);
    }

    /**
     * @return mixed
     */
    protected function listArtists()
    {
        return Artist::with('albums')->get();
    }

}