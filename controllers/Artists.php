<?php namespace Keios\StudioManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;

/**
 * Artists Back-end Controller
 */
class Artists extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * Artists constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.StudioManager', 'studiomanager', 'artists');
    }

    /**
     * Deleted checked artists.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $artistId) {
                if (!$artist = artist::find($artistId)) continue;
                $artist->delete();
            }

            Flash::success(Lang::get('keios.studiomanager::lang.artists.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.studiomanager::lang.artists.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}