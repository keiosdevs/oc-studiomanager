<?php namespace Keios\StudioManager\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;

/**
 * Albums Back-end Controller
 */
class Albums extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * Albums constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.StudioManager', 'studiomanager', 'albums');
    }

    /**
     * Deleted checked albums.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $albumId) {
                if (!$album = album::find($albumId)) continue;
                $album->delete();
            }

            Flash::success(Lang::get('keios.studiomanager::lang.albums.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.studiomanager::lang.albums.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}